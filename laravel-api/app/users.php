<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;


class users extends Model
{
    protected $table = 'users';
    protected $fillable = ['username' , 'email', 'name', 'role_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    public function roles()
    {
        return $this->hasMany('App\roles');
    }

    protected static function boot(){
        parent::boot();
    
        static::creating(function ($model){
            if(empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()} = Str::uuid();
            }
            });
        }
}
